-module(soapproxy_app).
-behaviour(application).

%% Application callbacks
-export([start/2]).
-export([stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_Type, _Args) ->
    Dispatch = cowboy_router:compile([
        %% {Host, [{URIPath, Handler, Opts}]}
        {'_', [
            {"/webservicex/[...]", webservicex_handler, []},
            {"/contentstuff/[...]", contentstuff_handler, []},
            {"/[...]", proxy_handler, []}
        ]}
    ]),
    {ok, _} = cowboy:start_http(listener, 100, [{port, 8080}], [
        {env, [{dispatch, Dispatch}]}
    ]),
    soapproxy_sup:start_link().

stop(_State) ->
    ok.
