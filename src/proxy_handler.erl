-module(proxy_handler).

-export([init/3]).
-export([handle/2]).
-export([terminate/3]).

init(_type, Req, _Opts) ->
    {ok, Req, []}.

handle(Req, State) ->
    % Read all the data from the request
    {Method, Req2} = cowboy_req:method(Req),
    {_Host, Req3} = cowboy_req:host(Req2),
    {Url, Req4} = cowboy_req:url(Req3),
    {Headers, Req5} = cowboy_req:headers(Req4),
    {ok, Body, Req6} = cowboy_req:body(Req5),

    % Proxy the request
    Opts = [async, {pool, default}, {recv_timeout, 15000}],
    %Opts = [{pool, default}, {recv_timeout, 15000}],
    Req7 = case make_request(Method, Url, Headers, Body, Opts) of
        {ok, ClientRef} ->
            case send_async_response(ClientRef, Req6, self(), Opts) of
                {ok, ReqX} ->
                    ReqX;
                Else ->
                    io:format("Unexpected send_response message ~p~n", [Else]),
                    Req6
                end;
        {ok, RespStatus, RespHeaders, ClientRef} ->
            send_response(ClientRef, Req6, {RespStatus, RespHeaders}, Opts);
        Else ->
            io:format("Unexpected send_response message ~p~n", [Else]),
            Req6
    end,
    {ok, Req7, State}.

terminate(_Reason, _Req, _State) ->
    ok.


%%
% Internal functions
make_request(Method, Url, Headers, Body, Opts) when is_binary(Url) ->
    UpdatedUrl = binary:bin_to_list(Url),
    UpdatedHeaders = normalize_headers(Headers, Body),
    make_request(Method, UpdatedUrl, UpdatedHeaders, Body, Opts);
make_request(<<"GET">>, Url, Headers, Body, Opts) ->
    hackney:request(get, Url, Headers, Body, Opts);
make_request(<<"POST">>, Url, Headers, Body, Opts) ->
    hackney:request(post, Url, Headers, Body, Opts);
make_request(<<"OPTIONS">>, Url, Headers, Body, Opts) ->
    hackney:request(options, Url, Headers, Body, Opts).

normalize_headers([{<<"content-length">>, _Length}|_Rest]=Headers, _Body) ->
    Headers;
normalize_headers(Headers, Body) ->
    ContentLength = erlang:integer_to_binary(
        string:len(
            binary:bin_to_list(Body))),
    [{<<"content-length">>, ContentLength}|Headers].

send_response(ClientRef, Request, Response, _Opts) ->
    {RespStatus, RespHeaders} = Response,
    RespBody = case hackney:body(ClientRef) of
        {ok, Content} ->
            Content;
        {error, {closed, Content}} ->
            Content
    end,
    cowboy_req:reply(RespStatus, RespHeaders, RespBody, Request).

send_async_response(ClientRef, Request, Parent, Acc) ->
    receive
        {hackney_response, ClientRef, {status, Code, Reason}} ->
            NewAcc = [{status, Code, Reason}|Acc],
            send_async_response(ClientRef, Request, Parent, NewAcc);

        {hackney_response, ClientRef, {headers, Headers}} ->
            [{status, RespStatus, _Reason}|_Rest] = Acc,
            {ok, NewReq} = cowboy_req:chunked_reply(RespStatus, Headers, Request),
            send_async_response(ClientRef, NewReq, Parent, []);

        {hackney_response, ClientRef, {error, _Reason}} ->
            % Erros, probably connection closed
            {ok, Request};

        {hackney_response, ClientRef, done} ->
            {ok, Request};

        {hackney_response, ClientRef, Bin} ->
            ok = cowboy_req:chunk(Bin, Request),
            send_async_response(ClientRef, Request, Parent, []);

        {cowboy_req, resp_sent} ->
            Parent ! {cowboy_req, resp_sent},
            send_async_response(ClientRef, Request, Parent, Acc);

        Else ->
            io:format("Unexpected message ~p~n", [Else]),
            {ok, Request}
    end.
