-module(contentstuff_handler).

-export([init/3]).
-export([handle/2]).
-export([terminate/3]).

init(_type, Req, _Opts) ->
    {ok, Req, []}.

handle(Req, State) ->
    % Read all the data from the request
    {Method, Req2} = cowboy_req:method(Req),
    {Url, Req3} = cowboy_req:url(Req2),
    {Headers, Req4} = cowboy_req:headers(Req3),
    {ok, Body, Req5} = cowboy_req:body(Req4),

    {ok, SUrl, SHeaders, Body} = translate_request(Url, Headers, Body),
    %io:format("requesting ~tp ~tp ~n, ", [SUrl, SHeaders]),

    % Proxy the request
    Opts = [{pool, default}, {recv_timeout, 15000}],
    Req6 = case make_request(Method, SUrl, SHeaders, Body, Opts) of
        {ok, RespStatus, RespHeaders, ClientRef} ->
            send_response(ClientRef, Req5, {RespStatus, RespHeaders}, Opts);
        Else ->
            io:format("Unexpected send_response message ~p~n", [Else]),
            Req5
    end,
    {ok, Req6, State}.

terminate(_Reason, _Req, _State) ->
    ok.


%%
% Internal functions
translate_request(Url, Headers, Body) ->
    SUrl = translate_to_server(Url),
    SHeaders = lists:keyreplace(<<"host">>, 1, Headers, {<<"host">>, <<"webservices.assinaja.com">>}),
    {ok, SUrl, SHeaders, Body}.
translate_response(Headers, Body) ->
    CBody = case lists:keysearch(<<"Content-Encoding">>, 1, Headers) of
        {value, {<<"Content-Encoding">>, <<"gzip">>}} ->
            zlib:gzip(translate_to_client(zlib:gunzip(Body)));
        _ ->
            translate_to_client(Body)
    end,
    {ok, Headers, CBody}.

translate_to_server(Subject) ->
    binary:replace(Subject,
        <<"http://sandbox.lancenet.com.br:8080/contentstuff">>,
        <<"https://webservices.assinaja.com">>,
        [global]).
translate_to_client(Subject) ->
    binary:replace(Subject,
        <<"https://webservices.assinaja.com">>,
        <<"http://sandbox.lancenet.com.br:8080/contentstuff">>,
        [global]).

make_request(Method, Url, Headers, Body, Opts) when is_binary(Url) ->
    UpdatedUrl = binary:bin_to_list(Url),
    UpdatedHeaders = normalize_headers(Headers, Body),
    make_request(Method, UpdatedUrl, UpdatedHeaders, Body, Opts);
make_request(<<"GET">>, Url, Headers, Body, Opts) ->
    hackney:request(get, Url, Headers, Body, Opts);
make_request(<<"POST">>, Url, Headers, Body, Opts) ->
    hackney:request(post, Url, Headers, Body, Opts);
make_request(<<"OPTIONS">>, Url, Headers, Body, Opts) ->
    hackney:request(options, Url, Headers, Body, Opts).

normalize_headers([{<<"content-length">>, _Length}|_Rest]=Headers, _Body) ->
    Headers;
normalize_headers(Headers, Body) ->
    ContentLength = erlang:integer_to_binary(
        string:len(
            binary:bin_to_list(Body))),
    [{<<"content-length">>, ContentLength}|Headers].

send_response(ClientRef, Request, Response, _Opts) ->
    {RespStatus, RespHeaders} = Response,
    RespBody = case hackney:body(ClientRef) of
        {ok, Content} ->
            Content;
        {error, {closed, Content}} ->
            Content
    end,

    {ok, RespHeaders, CBody} = translate_response(RespHeaders, RespBody),
    cowboy_req:reply(RespStatus, RespHeaders, CBody, Request).
