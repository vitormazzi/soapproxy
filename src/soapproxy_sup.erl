-module(soapproxy_sup).
-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%%%===================================================================
%%% API functions
%%%===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%%%===================================================================
%%% Supervisor callbacks
%%%===================================================================

%% There is no need to start processes, cowboy will handle that
init([]) ->
    Procs = [],
    {ok, {{one_for_one, 10, 10}, Procs}}.
